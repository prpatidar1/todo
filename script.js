var taskId=1;
var tasks= [];

var input=document.getElementById("todo_input");
var btn=document.getElementById("todo_save");
var todoItem=document.getElementById("bottom_div");

btn.addEventListener("click",function()
{	addTaskToArray();
});

function insertBlankLine(targetElement)
{
	var br = document.createElement("br");
	targetElement.appendChild(br);
}
function addTaskToArray()
{
	if(input.value){
		var taskObj={};
		taskObj.id=taskId;
		taskObj.name=input.value;
		input.value="";
		tasks.push(taskObj);
		addTaskToDom(taskObj);
		taskId++;
	}
}
function addTaskToDom(taskObj)
{	var newTask=document.createElement("div");
	newTask.setAttribute('id',taskObj.id);
	var taskName=document.createElement("text");
	taskName.innerHTML=taskObj.name;
	newTask.appendChild(taskName);
	
	var deleteTask=document.createElement("a");
	deleteTask.setAttribute("href","#");
	deleteTask.setAttribute("style","float:right;margin:0 12px 0 21px;text-decoration:none;color:red");
	deleteTask.innerHTML="remove";
	newTask.appendChild(deleteTask);
	
	deleteTask.addEventListener("click",function(event){
		var targetParent=event.target.parentNode;
		var selectTaskIndex=getTaskIndex(parseInt(targetParent.id));
		removeFromTaskArray(selectTaskIndex);
		targetParent.parentNode.removeChild(targetParent);
	});

	var check=document.createElement("input");
	check.setAttribute("type","checkbox"); 
	check.setAttribute("style","float:right"); 
	newTask.appendChild(check);
	check.addEventListener("change",function(event){
		if(check.checked){
			var targetParent=event.target.parentNode;
			var name=targetParent.firstChild;
			name=name.style.textDecoration = "line-through";
		}
		else{
			var targetParent=event.target.parentNode;
			var name=targetParent.firstChild;
			name=name.style.textDecoration = "none";
		}
	});

	var hr=document.createElement("hr");
	newTask.appendChild(hr);
	
	todoItem.appendChild(newTask);
}

function getTaskIndex(id){
	for(var i=0;i<tasks.length;i++)
	{
		if(tasks[i].id==id)
			return i;
	}
}
function removeFromTaskArray(index){
	tasks.splice(index,1);
}
